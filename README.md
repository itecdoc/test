## TASK №1

```
SET @minAge = 7;
SET @maxAge = 17;
SET @rangeDays = 14; 
SET @minMatches = 2;
SELECT u.id AS ID, CONCAT(u.first_name, ' ',  u.last_name) AS `Name`, b.author as Author, GROUP_CONCAT(b.`name` SEPARATOR ', ') AS Books
FROM user_books AS ub
INNER JOIN users AS u ON ub.user_id=u.id AND TIMESTAMPDIFF(YEAR, u.birthday, CURDATE()) >= @minAge AND TIMESTAMPDIFF(YEAR, u.birthday, CURDATE()) <= @maxAge
INNER JOIN books AS b ON ub.book_id=b.id
WHERE DATEDIFF(ub.return_date, ub.get_date) < @rangeDays 
GROUP BY CONCAT(u.first_name, ' ',  u.last_name), Author
HAVING COUNT(*) = @minMatches;
```

## TASK №2
### Introduction
This code is a test task. To run it you need **[DOCKER](https://docs.docker.com/engine/install/)** installed.

### Install

1. Create a folder, for example `test`;
2. Go to `test` folder;
3. Run command: `git clone "link to this repo" . `;
4. Check file .env This file contains envirents for Mysql connection and URL to getting rates and commissing value.
5. For starting project use next console command: `docker-compose up -d`;
6. In the next step, you need to configure the MySQL docker's container that you run in the last step. Run command: `docker ps` for get all running containers. Find container with name 'database' and her DATABASE_CONTAINER_ID and run console command: `
   docker exec -it DATABASE_CONTAINER_ID bash`. If everything goes well, you will be taken to the console inside the database image.
7. After you need run next commands bucket:
````  mysql -u root
   CREATE USER 'laravel'@'%' IDENTIFIED BY 'npeDd!sxD660'; //it is user and password from .env file
   CREATE DATABASE laravel; //this database name from .env file
   GRANT ALL ON laravel.* TO 'laravel'@'%';
   FLUSH PRIVILEGES;
   quit; //for exit from mysql
````
8. After you need connection inside app docker container:

   ````docker exec -it APP_CONTAINER_ID bash````
9. If everything goes well, run next commands:
````
cp .env.example .env
composer install
php artisan key:generate
php artisan migrate

php artisan db:seed --class=DatabaseSeeder //for creating demo user (test@example.com with password "123456789"
````
10. For get Bearer token you need have `curl` or postman client. For curl:
#### request and response:
````
POST
curl --location 'http://localhost/api/v1/tokens/create' \
--header 'Content-Type: application/json' \
--data-raw '{"email":"test@example.com","password":"123456789"}'
---------------
{"token":"_grC9DGUELNM1julVQZx8csCWS0REX7uabjWe7zew940de837"}
````
11. After that you can get rates. For this you need send request with responsed token:
````
GET
curl --location 'http://localhost/api/v1?method=rates&currency=USD' \
--header 'Authorization: Bearer _grC9DGUELNM1julVQZx8csCWS0REX7uabjWe7zew940de837' 
------------------
{"status":"success","code":200,"data":{"USD":57967.02}}
````
12. Example request without parameter 'currency' and response
````
GET
curl --location 'http://localhost/api/v1?method=rates' \
--header 'Authorization: Bearer _grC9DGUELNM1julVQZx8csCWS0REX7uabjWe7zew940de837' 
------------------
{"status":"success","code":200,"data":{"GBP":45370.21,"CHF":52071.38,"EUR":53552.39,"USD":58053.44,"SGD":78360.53,"CAD":79309.7,"AUD":85998.74,"NZD":94564.99,"RON":212827.15,"PLN":229326.12,"HRK":267781.06,"BRL":316983.39,"DKK":399465.72,"CNY":421978.84,"HKD":453655.7,"SEK":600297.82,"CZK":1346247.65,"TWD":1881424.89,"TRY":1893790.27,"THB":2114423.71,"INR":4845851.21,"RUB":5103246.63,"ISK":7589575.88,"JPY":9331219.58,"HUF":20988039.91,"ARS":52913029.27,"CLP":54128646.92,"KRW":79972095.95}}
````
13. Request/response example for convert BTC to USD:
````
POST
curl --location 'http://localhost/api/v1?method=convert' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer _grC9DGUELNM1julVQZx8csCWS0REX7uabjWe7zew940de837' \
--data '{"currency_from":"BTC","currency_to":"USD", "value":1}'
-------------------
{"status":"success","code":200,"data":{"currency_from":"BTC","currency_to":"USD","value":1,"converted_value":57985.61,"rate":57985.61}}
````
14. Request/response example for convert USD to BTC:
````
POST
curl --location 'http://localhost/api/v1?method=convert' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer _grC9DGUELNM1julVQZx8csCWS0REX7uabjWe7zew940de837' \
--data '{"currency_from":"USD","currency_to":"BTC", "value":60000}'
--------------------
{"status":"success","code":200,"data":{"currency_from":"USD","currency_to":"BTC","value":60000,"converted_value":1.0351070352,"rate":57965.02}}
````
15. Example request/response  with not correct data (USK it is not correct currency):
````
POST
curl --location 'http://localhost/api/v1?method=convert' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer _grC9DGUELNM1julVQZx8csCWS0REX7uabjWe7zew940de837' \
--data '{"currency_from":"USK","currency_to":"BTC", "value":60000}'
----------------
{"status":"error","code":400,"message":"Currency conversion problem"}
````


