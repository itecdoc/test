<?php

namespace App\Services;

use App\Http\Exceptions\BlockchainException;
use App\Services\Exchange\ExchangeServiceResponse;
use Brick\Math\BigDecimal;
use Brick\Math\RoundingMode;
use Symfony\Component\HttpFoundation\Response;

class IExchangeService implements ExchangeService
{

    /**
     * @var BlockchainDataService
     */
    private $blockchainDataService;

    public function __construct(BlockchainDataService $blockchainDataService)
    {
        $this->blockchainDataService = $blockchainDataService;
    }

    public function convertFromBTC(float $btcAmount, string $currency): ExchangeServiceResponse
    {
        $exchangeServiceData = $this->blockchainDataService->getData();
        if (array_key_exists(strtoupper($currency), $exchangeServiceData)) {

            $amount = BigDecimal::of($btcAmount)->multipliedBy($exchangeServiceData[$currency], 2)->toScale(2, RoundingMode::HALF_DOWN);

            return new ExchangeServiceResponse($amount->toFloat(), $exchangeServiceData[$currency]);

        } else {
            throw new BlockchainException("Currency not found", Response::HTTP_BAD_REQUEST);
        }
    }

    public function convertToBTC(float $amount, string $currency): ExchangeServiceResponse
    {
        $exchangeServiceData = $this->blockchainDataService->getData();
        if (array_key_exists(strtoupper($currency), $exchangeServiceData)) {

            $amount = BigDecimal::of($amount)->dividedBy($exchangeServiceData[$currency], 10, RoundingMode::HALF_DOWN);

            return new ExchangeServiceResponse($amount->toFloat(), $exchangeServiceData[$currency]);

        } else {
            throw new BlockchainException("Currency not found", Response::HTTP_BAD_REQUEST);
        }
    }


}
