<?php

namespace App\Services;

use App\Core\enums\Action;
use App\Core\Handlers\ConvertHandler;
use App\Core\Handlers\RateHandler;
use App\Http\Exceptions\HandlerException;
use App\Http\Requests\RateRequest;
use Symfony\Component\HttpFoundation\Response;

class CommandExecutorService
{
    public static function getHandler(RateRequest $request, BlockchainBitcoinService $service)
    {
        switch ($request->method) {
            case Action::RATES->value:
                return new RateHandler($request, $service);
            case Action::CONVERT->value:
                return new ConvertHandler($request, $service);
            default:
                throw new HandlerException ("Method not found", Response::HTTP_BAD_REQUEST);
        }
    }
}
