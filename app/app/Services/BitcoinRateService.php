<?php

namespace App\Services;

interface BitcoinRateService
{
    public function rate($currency = null);
}
