<?php

namespace App\Services;

use App\Http\Exceptions\BlockchainException;

interface BlockchainDataService
{
    public function getData(): array|BlockchainException;
}
