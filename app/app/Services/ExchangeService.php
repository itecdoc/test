<?php

namespace App\Services;
use App\Services\Exchange\ExchangeServiceResponse;

interface ExchangeService
{
    public function convertFromBTC(float $btcAmount, string $currency): ExchangeServiceResponse;

    public function convertToBTC(float $amount, string $currency): ExchangeServiceResponse;

}
