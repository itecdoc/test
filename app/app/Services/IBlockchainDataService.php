<?php

namespace App\Services;

use App\Http\Exceptions\BlockchainException;
use Brick\Math\BigDecimal;
use Brick\Math\RoundingMode;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;

class IBlockchainDataService implements BlockchainDataService
{
    protected $jsonKey = "last";

    public function getData(): array|BlockchainException
    {
        $commission = Config::integer('blockchain.commission');
        $url = Config::string('blockchain.url');
        $data = $this->getRatesRequest($url, 'GET');
        return $this->setCommission($data, $commission, $this->jsonKey);
    }

    private function getRatesRequest(string $url, $method = "GET"): array
    {

        $curl = curl_init($url);
        $arHeaderList = array();
        $arHeaderList[] = 'Accept: application/json';
        $arHeaderList[] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0';

        curl_setopt($curl, CURLOPT_HTTPHEADER, $arHeaderList);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_POST, $method === "GET" ? 0 : 1);
        $result = curl_exec($curl);
        if ($result === false) {
            throw new BlockchainException("Error receiving data", Response::HTTP_BAD_REQUEST);
        }
        curl_close($curl);
        $data = json_decode($result, true);
        if (json_last_error() === JSON_ERROR_NONE) {
            return $data;
        } else {
            throw new BlockchainException("The format of the received data is incorrect", Response::HTTP_BAD_REQUEST);
        }
    }

    private function setCommission(array $data, int $commision, string $jsonKey): array
    {
        $resData = array();
        foreach ($data as $cur => $values) {
            foreach ($values as $k => $v) {
                if ($k === $jsonKey) {
                    $amount = BigDecimal::of(floatval($v))->multipliedBy(100 + $commision)->dividedBy(100, 2, RoundingMode::HALF_DOWN);
                    $resData[$cur] = $amount->toFloat();
                }
            }
        }
        return $resData;
    }


}
