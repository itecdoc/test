<?php

namespace App\Services;

use App\Http\Exceptions\BlockchainException;
use App\Services\Exchange\ExchangeServiceResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class IBlockchainBitcoinService implements BlockchainBitcoinService
{
    protected $request;
    /**
     * @var BlockchainDataService
     */
    private $blockchainDataService;

    public function __construct(Request $request, BlockchainDataService $blockchainDataService, ExchangeService $exchangeService)
    {
        $this->request = $request;
        $this->blockchainDataService = $blockchainDataService;
        $this->exchangeService = $exchangeService;
    }

    public function convert(): array|BlockchainException
    {
        try {

            if (strtoupper($this->request->currency_from) === "BTC") {
                return $this->convertBTC();
            } else {
                return $this->convertCurrency();
            }
        } catch (\Exception $e) {
            throw new BlockchainException("Currency conversion problem", Response::HTTP_BAD_REQUEST);
        }
    }


    public function rate($currency = null): array|BlockchainException
    {
        try {
            $resData = $this->blockchainDataService->getData();
            if ($currency == null) {
                asort($resData);
                return $resData;
            } else {
                if (array_key_exists(strtoupper($currency), $resData)) {

                    return [$currency => $resData[$currency]];

                } else {
                    asort($resData);
                    return $resData;
                }
            }
        } catch (\Exception $e) {
            throw new BlockchainException("Data processing error", Response::HTTP_BAD_REQUEST);
        }

    }


    private function convertBTC(): array|BlockchainException
    {
        $btcAmount = floatval($this->request->value);
        $currency = $this->request->currency_to;

        /** @var ExchangeServiceResponse $exchangeResponse */
        $exchangeResponse = $this->exchangeService->convertFromBTC($btcAmount, $currency);

        return ['currency_from' => "BTC", "currency_to" => $currency, "value" => $btcAmount, "converted_value" => $exchangeResponse->getAmount(), "rate" => $exchangeResponse->getRate()];

    }

    private function convertCurrency(): array|BlockchainException
    {
        $currencyAmount = floatval($this->request->value);
        $currency = $this->request->currency_from;

        /** @var ExchangeServiceResponse $exchangeResponse */
        $exchangeResponse = $this->exchangeService->convertToBTC($currencyAmount, $currency);

        return ['currency_from' => $currency, "currency_to" => "BTC", "value" => $currencyAmount, "converted_value" => $exchangeResponse->getAmount(), "rate" => $exchangeResponse->getRate()];
    }
}
