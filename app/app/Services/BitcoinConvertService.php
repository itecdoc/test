<?php

namespace App\Services;

interface BitcoinConvertService
{
    public function convert();
}
