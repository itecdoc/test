<?php

namespace App\Services\Exchange;

class ExchangeServiceResponse
{
    private float $amount;
    private float $rate;

    /**
     * @param float $amount
     * @param float $rate
     */
    public function __construct($amount, $rate)
    {
        $this->amount = $amount;
        $this->rate = $rate;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

}
