<?php

namespace App\Http\Controllers;

use App\Http\Controllers\api\BaseController;
use App\Http\Requests\ConvertRequest;
use App\Http\Requests\RateRequest;
use App\Services\BlockchainBitcoinService;
use App\Services\CommandExecutorService;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;


class ExchangeRateController extends BaseController
{
    public function __construct(
        protected BlockchainBitcoinService $service,
    ) {
    }

    public function getRates(RateRequest $request,)
    {
        try {
            return CommandExecutorService::getHandler($request, $this->service)->execute();
        } catch (\Exception $e) {
            throw new HttpResponseException(response()->json([
                'status' => 'error',
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST));
        }
    }

    public function makeConvert(ConvertRequest $request)
    {
        try {
            return CommandExecutorService::getHandler($request, $this->service)->execute();
        } catch (\Exception $e) {
            throw new HttpResponseException(response()->json([
                'status' => 'error',
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST));
        }
    }

}
