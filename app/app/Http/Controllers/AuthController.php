<?php

namespace App\Http\Controllers;

use App\Http\Controllers\api\BaseController;
use App\Http\Requests\CreateTokenRequest;
use App\Models\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends BaseController
{

    /**
     * create token method by correct email and password.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreateTokenRequest $request)
    {

        /** @var User $user */
        $user = User::where('email', $request->email)->first();

        if ($user == null) {
            throw new HttpResponseException(response()->json([
                'errors' => "Email or password is not correct",
            ], Response::HTTP_FORBIDDEN));
        }

        if (!Hash::check($request->password, $user->password)) {
            throw new HttpResponseException(response()->json([
                'errors' => "Email or password is not correct",
            ], Response::HTTP_FORBIDDEN));
        }

        $user->tokens()->delete();
        $token = $user->createToken($user->name . '-AuthToken')->plainTextToken;

        return response()->json([
            'token' => $token,
        ]);
    }
}
