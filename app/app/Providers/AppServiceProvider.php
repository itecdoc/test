<?php

namespace App\Providers;

use App\Services\BlockchainBitcoinService;
use App\Services\BlockchainDataService;
use App\Services\ExchangeService;
use App\Services\IBlockchainBitcoinService;
use App\Services\IBlockchainDataService;
use App\Services\IExchangeService;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {

        $this->app->bind(BlockchainDataService::class, function ($app) {
            return new IBlockchainDataService();
        });
        $this->app->bind(ExchangeService::class, function ($app) {
            return new IExchangeService($app->make(BlockchainDataService::class));
        });
        $this->app->bind(BlockchainBitcoinService::class, function ($app) {
            return new IBlockchainBitcoinService($app->make(Request::class), $app->make(BlockchainDataService::class), $app->make(ExchangeService::class));
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
