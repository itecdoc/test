<?php

namespace App\Core\enums;

enum Action: string
{
    case RATES = 'rates';
    case CONVERT = 'convert';
}
