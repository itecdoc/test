<?php

namespace App\Core\Handlers;
Use Illuminate\Http\JsonResponse;

interface RequestHandler
{
    /**
     * execute command and return json Response
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function execute():JsonResponse;
}
