<?php

namespace App\Core\Handlers;

use App\Http\Requests\RateRequest;
use App\Services\BlockchainBitcoinService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class RateHandler implements RequestHandler
{

    private RateRequest $request;
    private BlockchainBitcoinService $blockchainBitcoinService;
    public function __construct(RateRequest $request, BlockchainBitcoinService $blockchainBitcoinService)
    {
        $this->request = $request;
        $this->blockchainBitcoinService = $blockchainBitcoinService;
    }

    public function execute(): JsonResponse
    {

        $currency = $this->request->currency ?: null;

        $data = $this->blockchainBitcoinService->rate($currency);


        return response()->json([
            'status' => 'success',
            'code' => Response::HTTP_OK,
            'data' => $data,
        ], Response::HTTP_OK);
    }

}
