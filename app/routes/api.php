<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ExchangeRateController;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

Route::group(array('prefix' => '/v1'), function () {
    Route::post('/tokens/create', [AuthController::class, 'create']);
    Route::get('/', [ExchangeRateController::class, 'getRates'])->middleware('auth:sanctum');
    Route::post('/', [ExchangeRateController::class, 'makeConvert'])->middleware('auth:sanctum');
});

Route::fallback(function () {
    return response()->json(
        [
            'status' => 'error',
            'code' => Response::HTTP_NOT_FOUND,
            'message' => 'Not Found'
        ],
        Response::HTTP_NOT_FOUND);
});

