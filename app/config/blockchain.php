<?php

return [

    'url' => env('BLOCKCHAIN_SERVICE_URL'),
    'commission' => intval(env('SERVICE_COMMISSION')),
];
