start:
	@docker-compose -f docker-compose.yml up -d
stop:
	@docker-compose -f docker-compose.yml down
restart:  start stop
version:
	@docker-compose -version